//
//  DetailReserveViewController.swift
//  TuParqueApp
//
//  Created by Juan Camilo Jaramillo on 18/04/20.
//  Copyright © 2020 Daniela Rocha. All rights reserved.
//

import UIKit
import Firebase

class DetailReserveViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var DateLabel: UILabel!
    @IBOutlet weak var reserveTable: UITableView!
    
    var strDate: String! = ""
    var horas: [String] = Constants.Horarios.horas
    private var reserves: [Reserve] = []
    private var ref: DatabaseReference!
    private var fecha: [String] = []
    private var day: String = ""
    private var month: String = ""
    private var year: String = ""
    private var user: String = ""
    var courtImgURL: String! = ""
    var courtId: String! = ""
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        ref = Database.database().reference()
        //ref.keepSynced(true)
        if let x = Auth.auth().currentUser?.email {
            self.user = x
        }
        if (!NetworkManager.isConnected()){
            let action = UIAlertAction(title: "Aceptar", style: .cancel, handler: { action in
                self.popToPark()
                
            })
            
            let alert = NetworkManager.displayConnectionAlert(action: action)
            
            self.present(alert,animated: true)
        }
        loadReserves()
        uploadData()
        self.loadView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.save()
        uploadData()
    }
    
    func uploadData(){
        // Do any additional setup after loading the view.
        let url = URL(string: courtImgURL!)
        let image = UIImage(named: "IDRD")
        // this downloads the image asynchronously if it's not cached yet
        imgView.kf.setImage(with: url, placeholder: image)
        
        fecha = strDate!.components(separatedBy: "/")
        day = fecha[0]
        month = fecha[1]
        year = fecha[2]
        
        DateLabel?.text = "Reservas para el "+"\(day)"+" de \(month)"+" del \(year)"
        
    }
    
    
    func loadReserves() {
        let defaults = UserDefaults.standard
        
        if(NetworkManager.isConnected()){
            print("Actualizando reservas")
            
            let conditionRef = self.ref.child("reserves")
            
            conditionRef.observeSingleEvent(of: .value) { (snap: DataSnapshot) in
                for childSnap in  snap.children.allObjects {
                    var courtId = ""
                    var time = ""
                    var user = ""
                    var comment = ""
                    var state = ""
                    var fec = [String] ()
                    var start = [String] ()
                    
                    let snap2 = childSnap as! DataSnapshot
                    
                    // Get date
                    let parkname = self.ref.child("reserves/\(snap2.key)/start")
                    parkname.observe(.value, with: { (snapshotname) in
                        time = (snapshotname.value as AnyObject).description as String
                        
                        // Get user
                        let parkname = self.ref.child("reserves/\(snap2.key)/user")
                        parkname.observe(.value, with: { (snapshotname) in
                            user = (snapshotname.value as AnyObject).description as String
                            
                            // Get state
                            let parkname = self.ref.child("reserves/\(snap2.key)/state")
                            parkname.observe(.value, with: { (snapshotname) in
                                state = (snapshotname.value as AnyObject).description as String
                                
                                // Get comment
                                let parkname = self.ref.child("reserves/\(snap2.key)/comment")
                                parkname.observe(.value, with: { (snapshotname) in
                                    comment = (snapshotname.value as AnyObject).description as String
                                    
                                    // Get courtID
                                    let parkname = self.ref.child("reserves/\(snap2.key)/courtId")
                                    parkname.observe(.value, with: { (snapshotname) in
                                        courtId = (snapshotname.value as AnyObject).description as String
                                        
                                        start = time.components(separatedBy: " ")
                                        fec = start[0].components(separatedBy: "/")
                                        
                                        
                                        let reserve = Reserve(hour: start[1], comment: comment, day: fec[0], month: fec[1], year: fec[2], state: state, courtId: courtId, user: user)
                                        
                                        self.reserves.append(reserve)
                                        
                                        self.reserveTable.reloadData()
                                    });
                                });
                            });
                        });
                    });
                }
            }
        }
        else{
            if let savedReserves = defaults.object(forKey: "reserves") as? Data {
                let jsonDecoder = JSONDecoder()
                
                do {
                    self.reserves = try jsonDecoder.decode([Reserve].self, from: savedReserves)
                } catch {
                    print("Failed to load reserves")
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return horas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = reserveTable.dequeueReusableCell(withIdentifier: "reserveCell", for: indexPath) as! ReserveTableViewCell
        let hora = horas[indexPath.row].components(separatedBy: ":")
        cell.horaLBL.text = hora[0] + ":" + hora[1]
        revisarBotones(boton: cell.reserveBtn, nombre: horas[indexPath.row])
        return cell
    }
    
    func revisarBotones(boton: UIButton, nombre: String){
        
        boton.accessibilityIdentifier = nombre
        boton.setTitleColor(UIColor.white, for: .normal)
        boton.contentRect(forBounds: CGRect(x: 100, y: 100, width: view.bounds.width, height: 50))
        boton.layer.cornerRadius = 20.0
        
        if(revisarReservas(hora: nombre)){
            boton.removeTarget(self, action: #selector(reservaOcupada), for: .touchUpInside)
            boton.addTarget(self, action: #selector(reservaOcupada), for: .touchUpInside)
            boton.backgroundColor = Constants.UX.orange
            boton.setTitle("Ocupado", for: .normal)
        }
        else{
            
            boton.setTitle("Disponible", for: .normal)
            boton.backgroundColor = Constants.UX.green
            boton.addTarget(self, action: #selector(hacerReserva), for: .touchUpInside)
        }
        
        
        
        
    }
    
    @objc func hacerReserva(sender: UIButton!) {
        if let buttonTitle = sender.accessibilityIdentifier {
            
            let hora = buttonTitle.components(separatedBy: ":")
            let alert = UIAlertController(title: "Realizar Reserva", message: "Desea reservar la cancha para las " + hora[0] + ":" + hora[1], preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Reservar", style: .cancel, handler: { action in
                let monthNumber = Constants.Fecha.dictionary[self.month] ?? self.month
                let reserve = Reserve(hour: buttonTitle, comment: "", day: self.day, month: monthNumber, year: self.year, state: "Reserved", courtId: self.courtId, user: self.user)
                
                self.escribirEnFirebase(reserva: reserve)
            }))
            alert.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: nil))
            
            self.present(alert, animated: true)
            
            
        }
        
    }
    
    @objc func reservaOcupada(sender: UIButton!) {
        if let buttonTitle = sender.accessibilityIdentifier {
            
            let hora = buttonTitle.components(separatedBy: ":")
            let alert = UIAlertController(title: "Realizar Reserva", message: "Ya existe una reserva para las " + hora[0] + ":" + hora[1], preferredStyle: .alert)
            
            
            alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
            
            self.present(alert, animated: true)
        }
        
    }
    
    func save() {
        let jsonEncoder = JSONEncoder()
        if let savedData = try? jsonEncoder.encode(reserves) {
            let defaults = UserDefaults.standard
            defaults.set(savedData, forKey: "reserves")
        } else {
            print("Failed to save reserves.")
        }
        
        
    }
    
    func revisarReservas(hora: String) -> Bool{
        for reserve in reserves{
            let fecha_num = Constants.Fecha.dictionary[month]!
            
            if(day.elementsEqual(reserve.day) && fecha_num.elementsEqual(reserve.month) && year.elementsEqual(reserve.year) && hora.elementsEqual(reserve.hour) && courtId.elementsEqual(reserve.courtId)) {
                return true
            }
        }
        
        return false
    }
    
    func escribirEnFirebase(reserva: Reserve){
        print("-----------------------------------")
        
        if (NetworkManager.isConnected()){
            self.reserves.append(reserva)
            self.save()
            
            let start = reserva.day+"/"+reserva.month+"/"+reserva.year+" "+reserva.hour
            let postInfo = [
                "comment": reserva.comment,
                "courtId": reserva.courtId,
                "user": reserva.user,
                "start": start,
                "state": reserva.state
            ]
            
            let reference  = ref.child("reserves").childByAutoId()
            
            reference.setValue(postInfo)
            Analytics.logEvent("reserve_court", parameters: [
            "reserve": reserva as NSObject
            ])
            
            let conf = UIAlertController(title: "Realizar Reserva", message: "Tu reserva ha sido exitosa", preferredStyle: .alert)
            if let _ = Constants.Courts.dictionary[courtId] {
                if let childautoID = reference.key {
                    Analytics.logEvent("reserve_court", parameters: [
                    "reserve": childautoID as NSString
                    ])
                    if let image = generateQRCode(from: "\(childautoID)") {
                        conf.addImage(image: image)
                        let uploadRef = Storage.storage().reference(withPath: "reserves/\(childautoID).jpg")
                        guard let imageData = image.jpegData(compressionQuality: 1) else {return}
                        let uploadMetaData = StorageMetadata.init()
                        uploadMetaData.contentType = "image/jpeg"
                        
                        let _ = uploadRef.putData(imageData, metadata: uploadMetaData) { (downoladMetaData, error) in
                            if let error = error {
                                print("Error \(error.localizedDescription)")
                                return
                            }
                            print("Successfull upload \(String(describing: downoladMetaData))")
                            
                        }
                    }
                    
                }
            }
            
            conf.addAction(UIAlertAction(title: "Aceptar", style: .cancel, handler: { action in
                self.popToPark()

            }))
            
            self.present(conf,animated: true)
        }
        else{
            
            let action = UIAlertAction(title: "Aceptar", style: .cancel, handler: { action in
                self.popToPark()
            })
            
            let alert = NetworkManager.displayConnectionAlert(action: action)
            self.present(alert,animated: true)
        }
        
    }
    
    func popToPark(){

        if let x = self.navigationController?.viewControllers[1] as? ParkViewController {
          self.navigationController?.popToViewController(x , animated: true)
        }
        
        else {
            self.navigationController?.popToViewController((self.navigationController?.viewControllers[1]) as! QRScannerViewController, animated: true)
        }

        
    }
    
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }
}
