//
//  HomeViewController.swift
//  TuParqueApp
//
//  Created by Daniela Rocha on 4/10/20.
//  Copyright © 2020 Daniela Rocha. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps
import GooglePlaces
import Firebase


class HomeViewController: UIViewController, GMSMapViewDelegate {
    
    let rootRef = Database.database().reference()
    var markers = [GMSMarker] ()
    var alert = UIAlertController()
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var userButton: UIButton!
    @IBOutlet weak var searchText: UITextField!
    
    var parques = [String:[String]]()
    var savedParques = [String:[String:String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Getting user's location
        mapView.settings.compassButton = true
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        mapView.delegate = self
        
        //rootRef.keepSynced(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if (NetworkManager.isConnected()){
            let conditionRef = rootRef.child("parks")
            conditionRef.observeSingleEvent(of: .value) { (snap: DataSnapshot) in
                //self.searchText.text = (snap.value as AnyObject).description
                
                let howmany = snap.children.allObjects.count
                for childSnap in  snap.children.allObjects {
                    let snap2 = childSnap as! DataSnapshot
                    var name = ""
                    var address = ""
                    var image = ""
                    var latitudeInt = 0.0
                    var longitudeInt = 0.0
                    var id = ""
                    var feat = [String]()
                    // Get name
                    let parkname = self.rootRef.child("parks/\(snap2.key)/name")
                    parkname.observe(.value, with: { (snapshotname) in
                        name = (snapshotname.value as AnyObject).description
                        
                        // Get address
                        let parkname = self.rootRef.child("parks/\(snap2.key)/address")
                        parkname.observe(.value, with: { (snapshotname) in
                            address = (snapshotname.value as AnyObject).description
                            
                            // Get address
                            let parkname = self.rootRef.child("parks/\(snap2.key)/imageURL")
                            parkname.observe(.value, with: { (snapshotname) in
                                image = (snapshotname.value as AnyObject).description
                                
                                // Get latitude
                                let latitude = self.rootRef.child("parks/\(snap2.key)/latitude")
                                latitude.observe(.value, with: { (snapshotname) in
                                    latitudeInt = ((snapshotname.value as AnyObject).description as NSString).doubleValue
                                    
                                    // Get longitude
                                    let longitude = self.rootRef.child("parks/\(snap2.key)/longitude")
                                    longitude.observe(.value, with: { (snapshotname) in
                                        longitudeInt = ((snapshotname.value as AnyObject).description as NSString).doubleValue
                                        let cord2D = CLLocationCoordinate2D(latitude: (latitudeInt), longitude: (longitudeInt))
                                        
                                        id = snap2.key
                                        feat.append(name)
                                        feat.append(address)
                                        feat.append(String(latitudeInt))
                                        feat.append(String(longitudeInt))
                                        feat.append(String(image))
                                        feat.append(String(id))
                                        
                                        self.parques[id] = feat
                                        
                                        let defaults = UserDefaults.standard
                                        defaults.set(self.parques, forKey: "parks")
                                        
                                        self.paintParks(cord2D, name, address, howmany, image, id)
                                    });
                                });
                            });
                        });
                    });
                }
            }
        }
        else{
            if let x = UserDefaults.standard.dictionary(forKey: "parks") {
                let howmany = x.keys.count
                for key in x.keys {
                    let arr : Array<String>  = x[key] as! Array<String>
                    let name = arr[0]
                    let address = arr[1]
                    let latitude  = Double(arr[2])
                    let longitude  = Double(arr[3])
                    let image = arr[4]
                    let id = arr[5]
                    let cord2D = CLLocationCoordinate2D(latitude: (latitude!), longitude: (longitude!))

                    self.paintParks(cord2D, name, address, howmany, image, id)

                }
            }
        }
        

    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(!NetworkManager.isConnected()){
            alert = NetworkManager.displayConnectionAlert(action: nil)
            
            self.present(self.alert, animated: true)
        }
    }
    
    
    @IBAction func locationTapped(_ sender: Any) {
        goToPlaces()
    }
    
    
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        print("Pressed ", marker.position)
    }
    func mapView(_ mapView: GMSMapView, didLongPressInfoWindowOf marker: GMSMarker) {
        print("Long Pressed ", marker.position)
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker)  -> Bool{

        let uData = marker.userData as! [String]

        let parkVC = (storyboard?.instantiateViewController(identifier: "ParkViewController") as?ParkViewController)!
        
        
        parkVC.parkName! = String(marker.title!)
        parkVC.parkImgURL! = String(uData[0])
        parkVC.parkID! = String(uData[1])
        
        self.navigationController?.pushViewController(parkVC, animated: true)
        
        return true
    }
    
    
    func paintParks(_ cord2D: CLLocationCoordinate2D, _ name: String, _ address : String, _ last : Int, _ image: String, _ id: String) {
        let marker = GMSMarker()
        marker.position =  cord2D
        marker.title = name
        marker.snippet = address
        marker.icon = GMSMarker.markerImage(with: Constants.UX.orange)
        var dic = [String] ()
        dic.append(image)
        dic.append(id)
        marker.userData = dic
        marker.map = self.mapView
        self.markers.append(marker)
        self.mapView.camera = GMSCameraPosition.camera(withTarget: cord2D, zoom: 9)
        
        // There are all the markers corresponding to the parks in the database
        if(last == self.markers.count) {
            for marker in self.markers {
                print(marker.title!)
                
            }
        }
    }
    
    func goToPlaces() {
        searchText.resignFirstResponder()
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        present(acController, animated: true, completion: nil)
    }
    
    
}
extension HomeViewController :CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue : CLLocationCoordinate2D = manager.location?.coordinate else {return}
        print("locations = \(locValue.latitude) \(locValue.longitude) ")
    }
}



extension HomeViewController: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(String(describing: place.name))")
        dismiss(animated: true, completion: nil)
        
        self.mapView.clear()
        self.searchText.text = place.name
        
        let cord2D = CLLocationCoordinate2D(latitude: (place.coordinate.latitude), longitude: (place.coordinate.longitude))
        
        let marker = GMSMarker()
        marker.position =  cord2D
        marker.title = "Location"
        marker.snippet = place.name
        
        let markerImage = UIImage(named: "icon_offer_pickup")!
        let markerView = UIImageView(image: markerImage)
        marker.iconView = markerView
        marker.map = self.mapView
        
        self.mapView.camera = GMSCameraPosition.camera(withTarget: cord2D, zoom: 15)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        
    }
    
    
}

