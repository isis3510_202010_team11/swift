//
//  QRScannerViewController.swift
//  TuParqueApp
//
//  Created by Daniela Rocha on 4/25/20.
//  Copyright © 2020 Daniela Rocha. All rights reserved.
//

import UIKit
import AVFoundation
import Foundation
import Firebase

class QRScannerViewController:UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    @IBOutlet weak var errorLabel: UILabel!
    let imagePicker = UIImagePickerController()
    let courtId : String = ""
    var ref : DatabaseReference!
    private var courts: [Court] = []
    @IBOutlet weak var imageView: UIImageView!
    var id : String = ""
    var parkName : String = ""
    var courtName : String = ""
    var courtImgUrl : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        errorLabel.alpha = 0
        ref = Database.database().reference()
        loadCourts()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        imageView.image = nil
        
    }
    
    @IBAction func pickImage(_ sender: Any) {
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerController.SourceType.savedPhotosAlbum
        self.present(imagePicker, animated:true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        errorLabel.alpha = 0
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageView.image = image
            if let features = detectQRCode(image), !features.isEmpty{
                for case let row as CIQRCodeFeature in features{
                    if let qrMsg = row.messageString {
                        print(qrMsg)
                        let arr = qrMsg.components(separatedBy: "ID:")
                        if arr.count == 2 {
                            self.id = arr[1]
                            print(qrMsg, self.id )
                            dismiss(animated: true, completion: nil)
                            let vc = (storyboard?.instantiateViewController(identifier: "ReservaViewController") as?ReservaViewController)!
                            let defaults = UserDefaults.standard
                            if let savedCourts = defaults.object(forKey: "courts") as? Data {
                                let jsonDecoder = JSONDecoder()
                                do {
                                    courts = try jsonDecoder.decode([Court].self, from: savedCourts)
                                } catch {
                                    print("Failed to load courts")
                                }
                            }
                            var halloUno = false
                            for court in courts {
                                if court.id == self.id {
                                    halloUno = true
                                    vc.parkName! = court.parkId
                                    vc.courtName! = court.name
                                    vc.courtImgURL! = court.image
                                    vc.courtId = self.id
                                    self.navigationController?.pushViewController(vc, animated: true)
                                    return
                                }
                            }
                            if !halloUno {
                                let image = UIImage(named: "error")
                                self.imageView.image = image
                                errorLabel.text = "Este QR no correponde a una de nuestras canchas"
                                errorLabel.alpha = 1
                                dismiss(animated: true, completion: nil)
                            }
                        }
                        else {
                            let image = UIImage(named: "error")
                            self.imageView.image = image
                            errorLabel.text = "Este QR no correponde a una de nuestras canchas"
                            errorLabel.alpha = 1
                            dismiss(animated: true, completion: nil)
                        }
                        
                    }
                    
                }
            }
            else {
                let image = UIImage(named: "error")
                self.imageView.image = image
                errorLabel.text = "Por favor carga una imagen que contenga un QR"
                errorLabel.alpha = 1
                dismiss(animated: true, completion: nil)
                
            }
        }
        
    }
    func detectQRCode(_ image: UIImage?) -> [CIFeature]? {
        if let image = image, let ciImage = CIImage.init(image: image){
            var options: [String: Any]
            let context = CIContext()
            options = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
            let qrDetector = CIDetector(ofType: CIDetectorTypeQRCode, context: context, options: options)
            if ciImage.properties.keys.contains((kCGImagePropertyOrientation as String)){
                options = [CIDetectorImageOrientation: ciImage.properties[(kCGImagePropertyOrientation as String)] ?? 1]
            } else {
                options = [CIDetectorImageOrientation: 1]
            }
            let features = qrDetector?.features(in: ciImage, options: options)
            return features
            
        }
        return nil
    }
    
    func loadCourts(){
        
        let defaults = UserDefaults.standard
        
        if let savedCourts = defaults.object(forKey: "courts") as? Data {
            
            let jsonDecoder = JSONDecoder()
            
            do {
                courts = try jsonDecoder.decode([Court].self, from: savedCourts)
            } catch {
                print("Failed to load courts")
            }
        }
        else {
            
            let conditionRef = ref.child("courts")
            conditionRef.observeSingleEvent(of: .value) { (snap: DataSnapshot) in
                for childSnap in  snap.children.allObjects {
                    let snap2 = childSnap as! DataSnapshot
                    var image = ""
                    var name = ""
                    var parkId = ""
                    var type = ""
                    
                    // Get imageURL
                    let parkname = self.ref.child("courts/\(snap2.key)/imageURL")
                    parkname.observe(.value, with: { (snapshotname) in
                        image = (snapshotname.value as AnyObject).description as String
                        
                        // Get name
                        let parkname = self.ref.child("courts/\(snap2.key)/name")
                        parkname.observe(.value, with: { (snapshotname) in
                            name = (snapshotname.value as AnyObject).description as String
                            
                            // Get parkId
                            let parkname = self.ref.child("courts/\(snap2.key)/parkId")
                            parkname.observe(.value, with: { (snapshotname) in
                                parkId = (snapshotname.value as AnyObject).description as String
                                
                                // Get type
                                let latitude = self.ref.child("courts/\(snap2.key)/type")
                                latitude.observe(.value, with: { (snapshotname) in
                                    type = (snapshotname.value as AnyObject).description as String
                                    
                                    let court = Court(id: snap2.key, image: image, name: name, parkId: parkId, type: type)
                                    
                                    self.courts.append(court)
                                    
                                    self.save()
                                    
                                });
                            });
                        });
                    });
                }
            }
        }
    }
    func save() {
        let jsonEncoder = JSONEncoder()
        if let savedData = try? jsonEncoder.encode(courts) {
            let defaults = UserDefaults.standard
            defaults.set(savedData, forKey: "courts")
        } else {
            print("Failed to save reserves.")
        }
    }
}

