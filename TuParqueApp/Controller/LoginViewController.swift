//
//  LoginViewController.swift
//  TuParqueApp
//
//  Created by Daniela Rocha on 4/10/20.
//  Copyright © 2020 Daniela Rocha. All rights reserved.
//

import UIKit
import FirebaseAuth
import CoreLocation
import Reachability

class LoginViewController: UIViewController {
    
    let locationManager = CLLocationManager()
    var alert = UIAlertController()
    let networkManager = NetworkManager()
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpElements()
        getCurrentLocation()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //declare this property where it won't go out of scope relative to your listener
        if isUserLoggedIn() {
            tranisitionToMap()
        }
        
        if (!NetworkManager.isConnected()){
            self.alert = NetworkManager.displayConnectionAlert(action: nil)
            print("a presentar")
            self.present(self.alert, animated: true)
            print("presenté")
        }
    }
    
    
    @IBAction func loginTapped(_ sender: Any) {
        let error = validateFields()
        
        if error != nil {
            showError(error!)
        }
        else {
            let email = emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            Auth.auth().signIn(withEmail:email  , password: password) { (result, error) in
                if error != nil {
                    self.showError(error!.localizedDescription )
                }
                else {
                    print("token",result?.self.user.refreshToken! ?? "")
                    self.tranisitionToMap()
                }
            }
        }
    }
    
    func isUserLoggedIn() -> Bool{
        print("loggeado", Auth.auth().currentUser != nil)
        return Auth.auth().currentUser != nil
    }
    
    
    func setUpElements() {
        errorLabel.alpha = 0
        
        Utilities.styleTextFieldWhite(emailTextField)
        Utilities.styleTextFieldWhite(passwordTextField)
        
    }
    
    func validateFields() -> String? {
        if emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""
        {
            return "Por favor completa todos los campos."
        }
        return nil
    }
    
    func showError(_ message: String) {
        if (!NetworkManager.isConnected()){
            self.alert = NetworkManager.displayConnectionAlert(action: nil)
            print("a presentar")
            self.present(self.alert, animated: true)
            print("presenté")
        }
        else{
            errorLabel.text = message
            errorLabel.alpha = 1
        }
    }
    
    // Going programatically to Home a.k.a Map
    func tranisitionToMap() {
        let homeViewController =
            storyboard?.instantiateViewController(identifier: "HomeNavigationController") as?LauncherViewController
        view.window?.rootViewController = homeViewController
        view.window?.makeKeyAndVisible()
    }
    
    func getCurrentLocation() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            //locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
}

extension LoginViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
}



