//
//  ParkViewController.swift
//  TuParqueApp
//
//  Created by Daniela Rocha on 4/23/20.
//  Copyright © 2020 Daniela Rocha. All rights reserved.
//

import UIKit
import Kingfisher
import Firebase

class ParkViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    var parkImgURL: String! = ""
    var parkName: String! = ""
    var parkID: String!  = ""
    var ref: DatabaseReference!
    private var courts: [Court] = []
    private var actualCourts: [Court] = []
    var tipo: String = "Deportivo"
    var parkImgCourt: String! = ""
    
    
    @IBOutlet weak var noDeportivasButton: UIButton!
    @IBOutlet weak var deportivasButton: UIButton!
    @IBOutlet weak var imgLabel: UIImageView!
    @IBOutlet weak var parkNavItem: UINavigationItem!
    @IBOutlet weak var courtsView: UICollectionView!
    
    
    @IBAction func deportivasTapped(_ sender: Any) {
        tipo = "Deportivo"
        actualizarTabla()
        self.loadView()
    }
    
    @IBAction func noDeportivasTapped(_ sender: Any) {
        tipo = "NoDeportivo"
        actualizarTabla()
        self.loadView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        parkNavItem.title = parkName
        
        ref = Database.database().reference()
        //ref.keepSynced(true)
        
        loadCourts()
        uploadData()
    }
    
    func uploadData(){
        let url = URL(string: parkImgURL!)
        //let image = UIImage(named: "IDRD")
        let processor = DownsamplingImageProcessor(size: CGSize(width: view.bounds.width, height: view.bounds.height))
        // this downloads the image asynchronously if it's not cached yet
        self.imgLabel.kf.indicatorType = .activity
        self.imgLabel.kf.setImage(with: url, options: [.processor(processor)])
    }
    
    override func viewDidAppear(_ animated: Bool) {
        uploadData()
        actualizarTabla()
    }
    
    func actualizarTabla(){
        actualCourts = []
        print("---------------------------------------------")
        print(courts)
        for court in courts{
            if (parkID.elementsEqual(court.parkId) && tipo.elementsEqual(court.type)){
                actualCourts.append(court)
            }
        }
        print("---------------------------------------------")
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return actualCourts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CourtCellView", for: indexPath) as! CourtCollectionViewCell
        
        let court = actualCourts[indexPath.item]
        
        let url = URL(string: court.image)
        let image = UIImage(named: "IDRD")
        let processor = DownsamplingImageProcessor(size: CGSize(width: imgLabel.bounds.width, height: imgLabel.bounds.height))
        // this downloads the image asynchronously if it's not cached yet
        cell.courtImage.kf.indicatorType = .activity
        cell.courtImage.kf.setImage(with: url, placeholder: image, options: [.processor(processor)])
        cell.courtName.text = court.name
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let court = actualCourts[indexPath.item]
        let vc = (storyboard?.instantiateViewController(identifier: "ReservaViewController") as?ReservaViewController)!
        vc.parkName! = self.parkName
        vc.courtName! = court.name
        vc.courtImgURL! = court.image
        vc.courtId = court.id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func loadCourts(){
        
        if (NetworkManager.isConnected()){
            let conditionRef = ref.child("courts")
            conditionRef.observeSingleEvent(of: .value) { (snap: DataSnapshot) in
                for childSnap in  snap.children.allObjects {
                    let snap2 = childSnap as! DataSnapshot
                    var image = ""
                    var name = ""
                    var parkId = ""
                    var type = ""
                    
                    // Get imageURL
                    let parkname = self.ref.child("courts/\(snap2.key)/imageURL")
                    parkname.observe(.value, with: { (snapshotname) in
                        image = (snapshotname.value as AnyObject).description as String
                        
                        // Get name
                        let parkname = self.ref.child("courts/\(snap2.key)/name")
                        parkname.observe(.value, with: { (snapshotname) in
                            name = (snapshotname.value as AnyObject).description as String
                            
                            // Get parkId
                            let parkname = self.ref.child("courts/\(snap2.key)/parkId")
                            parkname.observe(.value, with: { (snapshotname) in
                                parkId = (snapshotname.value as AnyObject).description as String
                                
                                // Get type
                                let latitude = self.ref.child("courts/\(snap2.key)/type")
                                latitude.observe(.value, with: { (snapshotname) in
                                    type = (snapshotname.value as AnyObject).description as String
                                    
                                    let court = Court(id: snap2.key, image: image, name: name, parkId: parkId, type: type)
                                    
                                    self.courts.append(court)
                                    
                                    self.save()
                                    
                                    DispatchQueue.main.async {
                                        self.actualizarTabla()
                                        self.loadView()
                                        print("DEBERIA RECARGAR")
                                    }
                                    
                                });
                            });
                        });
                    });
                }
            }
        }
        else{
            let defaults = UserDefaults.standard
            
            if let savedCourts = defaults.object(forKey: "courts") as? Data {
                
                let jsonDecoder = JSONDecoder()
                
                do {
                    courts = try jsonDecoder.decode([Court].self, from: savedCourts)
                } catch {
                    print("Failed to load courts")
                }
            }
            else{
                let alert = NetworkManager.displayConnectionAlert(action: nil)
                
                self.present(alert, animated: true)
            }
        }
        
    }
    
    func save() {
        let jsonEncoder = JSONEncoder()
        if let savedData = try? jsonEncoder.encode(courts) {
            let defaults = UserDefaults.standard
            defaults.set(savedData, forKey: "courts")
        } else {
            print("Failed to save reserves.")
        }
    }
    
}
