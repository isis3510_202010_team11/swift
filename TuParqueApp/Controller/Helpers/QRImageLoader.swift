//
//  QRImageLoader.swift
//  TuParqueApp
//
//  Created by Daniela Rocha on 4/26/20.
//  Copyright © 2020 Daniela Rocha. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class QRImageLoader {
    static let cache = NSCache<NSString,UIImage>()
    var alert = UIAlertController()
    
    static func loadImage(_ url: String, completion: @escaping ((UIImage) -> ())) {
        var image = UIImage()
        if(!NetworkManager.isConnected()){
            image = UIImage(named: "no-wifi") ?? UIImage()
            completion(image)
        }
        else {
            let storageRef = Storage.storage()
            let ref = storageRef.reference(withPath: "reserves")
            let child = ref.child(url)
            child.getData(maxSize: 1 * 1024 * 1024) { data, error in
                if let error = error {
                    print("Error: \(error.localizedDescription)")
                    image = UIImage()
                } else {
                    if let y = UIImage(data: data!) {
                        print("Here's my image",y)
                        image = y
                        cache.setObject(image, forKey: url as NSString)
                    }
                }
                DispatchQueue.main.async {
                    print("Here's my image 2.0",image)
                    completion(image)
                }
            }
        }
    }
    
    static func getImage(_ url: String, completion: @escaping ((UIImage) -> ())) {
        if(!NetworkManager.isConnected()){
           let image = UIImage(named: "no-wifi") ?? UIImage()
            completion(image)
        }
        else {
            if let image = cache.object(forKey: url as NSString? ?? NSString()) {
                completion(image)
            }
            else {
                loadImage(url, completion: completion)
            }
        }
        

    }
}
