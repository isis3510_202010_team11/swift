//
//  Constants.swift
//  TuParqueApp
//
//  Created by Daniela Rocha on 4/10/20.
//  Copyright © 2020 Daniela Rocha. All rights reserved.
//

import Foundation
import UIKit
struct Constants {
    struct Storyboard {
        static let homeViewController = "HomeVC"
        static let parkViewController = "ParkVC"
        static let loginViewController = "LoginVC"
        static let parkNavigationController = "ParkNC"
        static let qrViewController = "qrVC"
        
    }
    struct UX {
        
        static let green = UIColor(red: 98/255, green: 200/255, blue: 213/255, alpha: 1)
        static let orange = UIColor(red: 255/255, green: 182/255, blue: 54/255, alpha: 1)
        static let gray = UIColor(red: 237/255, green: 237/255, blue: 237/255, alpha: 1)
        static let lightGray = UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 1)
        static let darkGray = UIColor(red: 200/255, green: 200/255, blue: 200/255, alpha: 1)
    }
    
    struct Courts {
        static let dictionary: [String:String] = ["0": "Campo de Fútbol", "1": "Coliseo" ,"2":"Cancha micro", "3":"Cancha baloncesto", "4":"Cancha tenis", "5":"Pista patinaje", "6":"Bicicross", "7":"Yoga",]
    }
    struct Fecha {
        static let dictionary: [String:String] = ["enero": "01", "febrero": "02" ,"marzo":"03", "abril":"04", "mayo":"05", "junio":"06", "julio":"07", "agosto":"08", "septiembre": "09", "octubre": "10", "noviembre": "11", "diciembre": "12", "1": "enero", "2": "febrero" ,"3":"marzo", "4":"abril", "5":"mayo", "6":"junio", "7":"julio", "8":"agosto", "9": "septiembre", "10": "octubre", "11": "noviembre", "12": "diciembre",]
    }
    struct Horarios{
        static let horas: [String] = ["8:00:00","9:00:00","10:00:00","11:00:00","12:00:00","13:00:00","14:00:00","15:00:00","16:00:00","17:00:00","18:00:00"]
    }

}
