//
//  AlertImage.swift
//  TuParqueApp
//
//  Created by Daniela Rocha on 4/25/20.
//  Copyright © 2020 Daniela Rocha. All rights reserved.
//

import UIKit

extension UIAlertController {
    func addImage(image:UIImage) {
        let maxsize = CGSize(width: 205, height: 260)
        let imgSize = image.size
        var ratio : CGFloat!
        print("dimensioneees")
        print(imgSize.width, imgSize.height)
        if(imgSize.width > imgSize.height) {
            ratio = maxsize.width / imgSize.width
        }
        else {
            ratio = maxsize.height / imgSize.height
        }
        let scaledSize = CGSize(width: imgSize.width * ratio, height: imgSize.height * ratio)
        let imgAction = UIAlertAction(title: "", style: .default, handler: nil)
        var resizedImage = image.imageWithSize(scaledSize)
        
        
        if(imgSize.height >= imgSize.width) {
            resizedImage = resizedImage.withAlignmentRectInsets(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        }
        
        imgAction.isEnabled = false
        imgAction.setValue(resizedImage.withRenderingMode(.alwaysOriginal), forKey: "image")
        self.addAction(imgAction)
    }
}
