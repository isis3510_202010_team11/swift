//
//  NetworkManager.swift
//  TuParqueApp
//
//  Created by Daniela Rocha on 4/18/20.
//  Copyright © 2020 Daniela Rocha. All rights reserved.
//

import Foundation
import Reachability

class NetworkManager: NSObject {
    var reachability: Reachability!
    
    static let sharedInstance: NetworkManager = {
        return NetworkManager()
    }()
    override init() {
        super.init()
        // Initialise reachability
        reachability = try! Reachability()
        // Register an observer for the network status
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(networkStatusChanged(_:)),
            name: .reachabilityChanged,
            object: reachability
        )
        do {
            // Start the network status notifier
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    @objc func networkStatusChanged(_ notification: Notification) {
        // Do something globally here!
    }
    static func stopNotifier() -> Void {
        do {
            // Stop the network status notifier
            try (NetworkManager.sharedInstance.reachability).startNotifier()
        } catch {
            print("Error stopping notifier")
        }
    }
    
    // Network is reachable
    static func isReachable(completed: @escaping (NetworkManager) -> Void) {
        if (NetworkManager.sharedInstance.reachability).connection != .unavailable {
            completed(NetworkManager.sharedInstance)
        }
    }
    // Network is unreachable
    static func isUnreachable(completed: @escaping (NetworkManager) -> Void) {
        if (NetworkManager.sharedInstance.reachability).connection == .unavailable {
            completed(NetworkManager.sharedInstance)
        }
    }
    // Network is reachable via WWAN/Cellular
    static func isReachableViaWWAN(completed: @escaping (NetworkManager) -> Void) {
        if (NetworkManager.sharedInstance.reachability).connection == .cellular {
            completed(NetworkManager.sharedInstance)
        }
    }
    // Network is reachable via WiFi
    static func isReachableViaWiFi(completed: @escaping (NetworkManager) -> Void) {
        if (NetworkManager.sharedInstance.reachability).connection == .wifi {
            completed(NetworkManager.sharedInstance)
        }
    }
    
    static func isConnected() -> Bool {
        var res: Bool = false
        
        NetworkManager.isReachable { _ in
            print("SÍ HAY RED")
            res = testConnectivity()
        }
        
        return res
        
    }
    
    static func testConnectivity() -> Bool {
        let reachability = try! Reachability()
        var conn : Bool = false
        print("Reachability")
        reachability.whenReachable = { reachability in
            if reachability.connection == .wifi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
            conn = true
        }
        reachability.whenUnreachable = { _ in
            print("Not reachable")
            conn = false
        }
        
        do {
            try reachability.startNotifier()
            print("TRYING.....")
            conn = true
            
        } catch {
            print("Unable to start notifier")
            conn = false
        }
        
        return conn
    }
    
    static func displayConnectionAlert(action: UIAlertAction?) -> UIAlertController{
        let alert = UIAlertController(title: "¡No estás conectado a internet!", message: "Verifica tu conexión a internet para poder continuar.", preferredStyle: .alert)
        if action == nil {
            alert.addAction(UIAlertAction(title: "Aceptar", style: .cancel, handler: nil))
        }
        else{ alert.addAction(action!) }
        

        return alert
    }
    
}
