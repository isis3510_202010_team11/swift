//
//  SignUpViewController.swift
//  TuParqueApp
//
//  Created by Daniela Rocha on 4/10/20.
//  Copyright © 2020 Daniela Rocha. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class SignUpViewController: UIViewController {
    
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordConfTextField: UITextField!
    @IBOutlet weak var SignUpButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    
    @IBOutlet weak var genderField: UITextField!
    @IBOutlet weak var birthdateField: UITextField!
    
    
    private var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        setUpElements()
        // Do any additional setup after loading the view.
    }
    
    func setUpElements() {
        errorLabel.alpha = 0
        
        //Style text fields
        Utilities.styleTextFieldBlack(nameTextField)
        Utilities.styleTextFieldBlack(emailTextField)
        Utilities.styleTextFieldBlack(passwordTextField)
        Utilities.styleTextFieldBlack(passwordConfTextField)
        Utilities.styleTextFieldBlack(genderField)
        Utilities.styleTextFieldBlack(birthdateField)
        
    }
    
    @IBAction func signUpTapped(_ sender: Any) {
        // Validate fields
        let error = validateFields()
        
        if error != nil {
            showError(error!)
        }
        else {
            let email = emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            // Create user
            Auth.auth().createUser(withEmail: email, password: password) { (result, err) in
                if err != nil {
                    if let err = err {
                        let description = err.localizedDescription
                        print("ERROR:",description)
                        self.showError("Hubo un error creando el usuario. Porfavor vuelva a intentarlo")
                    }
                    
                }
                else {
                    let postInfo = [
                        "mail":self.emailTextField.text!,
                        "name":self.nameTextField.text!,
                        "birthdate":self.birthdateField.text!,
                        "gender":self.genderField.text!
                    ]
                    let reference  = self.ref.child("users").childByAutoId()
                    reference.setValue(postInfo)
                    self.tranisitionToMap()
                }
            }
        }
    }
    
    func validateFields() -> String? {
        
        if nameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            passwordConfTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            genderField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            birthdateField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""
        {
            return "Por favor completa todos los campos."
        }
        else if(!isValidEmail(emailTextField.text!)) {
            return "El email ingresado no es válido."
        }
        else if passwordTextField.text != passwordConfTextField.text {
            return "No coincide la contraseña y su confirmación."
        }
        else if(!isValidBirthDate(birthdateField.text!)) {
            return "Introduzca la fecha en el formato específicado."
        }
        else if(!isValidGender(genderField.text!)) {
            return "Introduzca su género en el formato específicado."
        }
        else if(passwordTextField.text!.count<6) {
            return "Su contraseña debe tener como mínimo 6 caracteres."
        }
        return nil
    }
    
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    func isValidBirthDate(_ date: String) -> Bool {
        let dateRegEx = "[0-9]{1,2}(/)[0-9]{1,2}(/)[0-9]{4}"
        let datePred = NSPredicate(format:"SELF MATCHES %@", dateRegEx)
        return datePred.evaluate(with: date)
    }
    func isValidGender(_ gender: String) -> Bool {
        let genderRegEx = "(F|M|Otro)"
        let genderPred = NSPredicate(format:"SELF MATCHES %@", genderRegEx)
        return genderPred.evaluate(with: gender)
    }
    
    func showError(_ message: String) {
        errorLabel.text = message
        errorLabel.alpha = 1
    }
    
    func tranisitionToMap() {
        let homeViewController =
            storyboard?.instantiateViewController(identifier: "HomeNavigationController") as?LauncherViewController
        view.window?.rootViewController = homeViewController
        view.window?.makeKeyAndVisible()
    }
    
}
