//
//  ReservaViewController.swift
//  TuParqueApp
//
//  Created by Juan Camilo Jaramillo on 18/04/20.
//  Copyright © 2020 Daniela Rocha. All rights reserved.
//

import UIKit
import Kingfisher

class ReservaViewController: UIViewController {


    @IBOutlet weak var reservasNavItem: UINavigationItem!
    @IBOutlet weak var imgLabel: UIImageView!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var dateReserve: UIDatePicker!
    
    private var date : String! = ""
    var courtImgURL : String! = ""
    var parkName : String! = ""
    var courtName : String! = ""
    var courtId: String = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        reservasNavItem.title = courtName
        let url = URL(string: courtImgURL!)

        let image = UIImage(named: "IDRD")
        // this downloads the image asynchronously if it's not cached yet
        imgLabel.kf.setImage(with: url!, placeholder: image)
        
        dateReserve.minimumDate = Date()
        dateReserve.setDate(Date(), animated: true)
        let ini = Date()
        setDate(newDate:ini)
    }
 
    func setDate(newDate: Date){
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "es_CO")
        dateFormatter.dateFormat = "dd/MMMM/yyyy"

        let strDate = dateFormatter.string(from: newDate)
        date = strDate
    }
    
    @IBAction func datePickerChanged(_ sender: Any) {

        setDate(newDate: dateReserve.date)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        
        if segue.destination is DetailReserveViewController
        {
            
            let vc = segue.destination as? DetailReserveViewController

            vc?.strDate = date!
            vc?.courtImgURL = self.courtImgURL
            vc?.courtId = self.courtId
        }
    }
    
    
}
