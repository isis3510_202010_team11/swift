//
//  UserAccountViewController.swift
//  TuParqueApp
//
//  Created by Daniela Rocha on 4/23/20.
//  Copyright © 2020 Daniela Rocha. All rights reserved.
//

import UIKit
import Firebase

class UserAccountViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    var ref: DatabaseReference!
    let rootRef = Database.database().reference()
    var user = ""
    var bookings = [String]()
    var url = ""
    var text = ""
    var accessibility = ""
    var hour = ""
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var reservas: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let x = Auth.auth().currentUser?.email {
            self.emailLabel.text = x
            user = x
        }
        
    }
    
    @IBAction func logout(_ sender: Any) {
        let firebaseAuth = Auth.auth()
        tranisitionToLogin()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.bookings = []
        getMyBookings()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("aiudaaaaa",self.bookings.count)
        if let x = UserDefaults.standard.dictionary(forKey: "myRes") {
            if(x.count == 0 ) {
                
                return 1
            }
            else {
                return self.bookings.count
            }
        }
        return self.bookings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = reservas.dequeueReusableCell(withIdentifier: "myReserveCell", for: indexPath) as! MyReserveTableViewCell
        let label = UIButton(frame: CGRect(x: 100, y: 100, width: view.bounds.width, height: 50))
        let cellHeight: CGFloat = 44.0
        label.center = CGPoint(x: view.bounds.width / 2.0, y: cellHeight / 2.0)
        if self.bookings.count == 0 {
            label.setTitle("Aún no tienes ninguna reserva. ¡Anímate!", for: .normal)
        }
        else {
            for cell in cell.subviews {
                cell.removeFromSuperview()
            }
            if reservaPasada(self.bookings[indexPath.row]){
                label.isEnabled = false
                label.setTitleColor(Constants.UX.darkGray, for: .normal)
                label.backgroundColor = Constants.UX.lightGray
                print("enabled?")
            }
            else {
                label.setTitleColor(UIColor.black, for: .normal)
                label.backgroundColor = Constants.UX.gray
            }
            
            self.accessibility = self.bookings[indexPath.row]
            label.accessibilityIdentifier = accessibility
            label.addTarget(self, action: #selector(irAQR), for: .touchUpInside)
            label.setTitle(text, for: .normal)
            
        }
        cell.addSubview(label)
        return cell
    }
    
    @objc func irAQR(sender: UILabel!) {
        if let reserveId = sender.accessibilityIdentifier {
            let arr = reserveId.components(separatedBy: " - ")
            self.text = arr[0]
            self.url = "\(arr[1]).jpg"
            tranisitionToQRScanner()
            
        }
        
    }
    
    func reservaPasada(_ cellIdentifier : String) -> Bool {
        let now = Date().addingTimeInterval(-18000)
        let arr = cellIdentifier.components(separatedBy: " - ")
        self.text = arr[0]
        let arr2 = arr[0].components(separatedBy: " : ")
        var datee = arr2[1]
        datee = datee.replacingOccurrences(of: "/", with: "-")
        let arr3 = datee.components(separatedBy: " ")
        var hourr = arr3[1]+"+0000"
        if !hourr.starts(with: "1") {
            hourr="0\(hourr)"
        }
        datee = "\(arr3[0])T\(hourr)"
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "dd-MM-yyyy'T'HH:mm:ssZ"
        let date = dateFormatter.date(from:datee)!
        if date < now {
            return true
        }
        else {
            return false
        }
    }
    
    func getMyBookings() {
        if (NetworkManager.isConnected()){
            let conditionRef = self.rootRef.child("reserves")
            conditionRef.observeSingleEvent(of: .value) { (snap: DataSnapshot) in
                for childSnap in  snap.children.allObjects {
                    let snap2 = childSnap as! DataSnapshot
                    var courtName = ""
                    var startH = ""
                    var email = ""
                    // Get users reserves
                    let emailF = self.rootRef.child("reserves/\(snap2.key)/user")
                    emailF.observe(.value, with: { (snapshotname) in
                        email = (snapshotname.value as AnyObject).description
                        if email == self.user {
                            // Get courtId
                            let court = self.rootRef.child("reserves/\(snap2.key)/courtId")
                            court.observe(.value, with: { (snapshotname) in
                                if let y = (snapshotname.value as AnyObject).description{
                                    courtName = Constants.Courts.dictionary[y] ?? ""
                                }
                                // Get date
                                let start = self.rootRef.child("reserves/\(snap2.key)/start")
                                start.observe(.value, with: { (snapshotname) in
                                    startH = (snapshotname.value as AnyObject).description
                                    let reserve = "\(courtName) : \(startH) - \(snap2.key)"
                                    self.bookings.append(reserve)
                                    let defaults = UserDefaults.standard
                                    defaults.set(self.bookings, forKey: "myRes")
                                    self.reservas.reloadData()
                                    
                                    
                                    
                                });
                            });
                        }
                    });
                }
            }
        }
        else {
            if let x = UserDefaults.standard.array(forKey: "myRes") {
                self.bookings = x as! [String]
                print("no habia internet ajaaa",self.bookings)
            }
        }
        
    }
    
    
    func tranisitionToLogin() {
        let loginViewController =
            storyboard?.instantiateViewController(identifier: Constants.Storyboard.loginViewController) as?LoginViewController
        
        view.window?.rootViewController = loginViewController
        view.window?.makeKeyAndVisible()
    }
    
    func tranisitionToQRScanner() {
        if let vc =
            storyboard?.instantiateViewController(identifier: Constants.Storyboard.qrViewController) as?QRViewController {
            let arr = self.text.components(separatedBy: " : ")
            vc.url = self.url
            vc.park = arr[0]
            vc.hour = arr[1]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}



