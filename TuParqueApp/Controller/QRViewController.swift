//
//  QRViewController.swift
//  TuParqueApp
//
//  Created by Daniela Rocha on 4/25/20.
//  Copyright © 2020 Daniela Rocha. All rights reserved.
//

import UIKit
import Firebase
class QRViewController: UIViewController {
    
    @IBOutlet weak var parkN: UILabel!
    @IBOutlet weak var reserveH: UILabel!
    @IBOutlet weak var qrImage: UIImageView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var cancelReserveBtn: UIButton!
    var alert = UIAlertController()
    var url : String = ""
    var park : String = ""
    var hour : String = ""
    var myImage = UIImage()
    var reserveId : String = ""
    var ref : DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        errorLabel.alpha = 0
        Utilities.styleFilledButton(cancelReserveBtn)
        reserveId = url.replacingOccurrences(of: ".jpg", with: "")
        ref = Database.database().reference()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        if(!NetworkManager.isConnected()){
            alert = NetworkManager.displayConnectionAlert(action: nil)
            self.present(self.alert, animated: true)
        }
        
        parkN.text = park
        reserveH.text = hour
        QRImageLoader.getImage(url) { image in
            guard image.jpegData(compressionQuality: 1) != nil
                else {
                    self.errorLabel.alpha = 1
                    let image = UIImage(named: "error")
                    self.qrImage.image = image
                    return
            }
            let image = image
            self.qrImage.image = image
        }
    }
    
    @IBAction func cancelPicked(_ sender: Any) {
        if(!NetworkManager.isConnected()){
            alert = NetworkManager.displayConnectionAlert(action: nil)
            
            self.present(self.alert, animated: true)
        }
        else {
            let alert2 = UIAlertController(title: "Cancelar reserva", message: "¿Deseas cancelar tu reserva?", preferredStyle: .alert)
            
            alert2.addAction(UIAlertAction(title: "Sí", style: .cancel, handler: { action in
                self.ref.child("reserves").child(self.reserveId).removeValue(completionBlock: { (error, refer) in
                    if let error = error {
                        print(error.localizedDescription)
                    } else {
                        print(refer)
                        print("Child Removed Correctly")
                        self.navigationController?.popToViewController((self.navigationController?.viewControllers[0]) as! HomeViewController, animated: true)
                    }
                })
            }))
            alert2.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
            
            self.present(alert2, animated: true)
        }
        
        
        
        
    }
}
