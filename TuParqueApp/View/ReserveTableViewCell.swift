//
//  ToDoTableViewCell.swift
//  CoreDataToDo
//
//  Created by Kenta Kodashima on 2019-03-14.
//  Copyright © 2019 Kenta Kodashima. All rights reserved.
//

import UIKit

class ReserveTableViewCell: UITableViewCell {
    
    @IBOutlet weak var reserveBtn: UIButton!
    @IBOutlet weak var horaLBL: UILabel!
}
