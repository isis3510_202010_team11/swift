//
//  CourtCollectionViewCell.swift
//  TuParqueApp
//
//  Created by Juan Camilo Jaramillo on 23/04/20.
//  Copyright © 2020 Daniela Rocha. All rights reserved.
//

import UIKit

class CourtCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var courtImage: UIImageView!
    
    @IBOutlet weak var courtName: UILabel!
    
}
