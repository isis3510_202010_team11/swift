//
//  Court.swift
//  TuParqueApp
//
//  Created by Juan Camilo Jaramillo on 23/04/20.
//  Copyright © 2020 Daniela Rocha. All rights reserved.
//

import UIKit


class Court: NSObject, Codable {
    var id: String
    var image: String
    var name: String
    var parkId: String
    var type: String

    init(id: String, image: String, name: String, parkId: String, type: String) {
        self.id = id
        self.image = image
        self.name = name
        self.parkId = parkId
        self.type = type
    }
}
