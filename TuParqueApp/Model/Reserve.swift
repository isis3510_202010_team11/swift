//
//  Reserve+CoreDataClass.swift
//  
//
//  Created by Juan Camilo Jaramillo on 4/04/20.
//
//

import UIKit


class Reserve: NSObject, Codable {
    var day: String
    var month: String
    var year: String
    var hour: String
    var comment: String
    var state: String
    var courtId: String
    var user: String

    init(hour: String, comment: String, day: String, month: String, year: String, state: String, courtId: String, user: String) {
        self.day = day
        self.month = month
        self.year = year
        self.hour = hour
        self.comment = comment
        self.state = state
        self.courtId = courtId
        self.user = user
    }
}
