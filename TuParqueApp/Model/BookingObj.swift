//
//  BookingRow.swift
//  TuParque
//
//  Created by Daniela Rocha on 2/29/20.
//  Copyright © 2020 Daniela Rocha. All rights reserved.
//

import SwiftUI

struct BookingObj: Hashable, Codable, Identifiable  {
    var id: Int
    var hour: String
    var state: String
}

