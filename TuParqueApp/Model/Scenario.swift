//
//  Scenario.swift
//  TuParque
//
//  Created by Daniela Rocha on 2/29/20.
//  Copyright © 2020 Daniela Rocha. All rights reserved.
//

import SwiftUI
import CoreLocation

struct Scenario: Hashable, Codable, Identifiable {
    var id: Int
    var name: String
    var city: String
    fileprivate var imageName: String
    fileprivate var coordinates: Coordinates
    var park: String
    var category: Category
    var isSporty: Bool
    var isFeatured: Bool

    var locationCoordinate: CLLocationCoordinate2D {
        CLLocationCoordinate2D(
            latitude: coordinates.latitude,
            longitude: coordinates.longitude)
    }

    enum Category: String, CaseIterable, Codable, Hashable {
        case deportivos = "Deportivo"
        case noDeportivos = "No deportivo"
    }
}

extension Scenario {
    var image: Image {
        ImageStore.shared.image(name: imageName)
    }
}

struct Coordinates: Hashable, Codable {
    var latitude: Double
    var longitude: Double
}
