//
//  Reserve+CoreDataProperties.swift
//  TuParqueApp
//
//  Created by Juan Camilo Jaramillo on 5/04/20.
//  Copyright © 2020 Daniela Rocha. All rights reserved.
//
//

import Foundation
import CoreData


extension Reserve {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Reserve> {
        return NSFetchRequest<Reserve>(entityName: "Reserve")
    }

    @NSManaged public var comment: String?
    @NSManaged public var reserveHour: String?
    @NSManaged public var dateCreated: Date?

}
